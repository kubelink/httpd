## Apache HTTP server Docker image
### Non-root Docker image running Centos Linux and httpd

### Base docker image

 - [hub.docker.com](https://hub.docker.com/r/eeacms/apache)

### Source Repository

- [GitLab](https://gitlab.com/kubelink/httpd)

### Environment Variables

| Variable           | Description                                                                                | Default           |
|--------------------|--------------------------------------------------------------------------------------------|-------------------|
| `DOMAIN_NAME`      | Domain name as it appears in logs                                                          | ``                |
| `PHP_FPM_HOST`     | If set then php steam will be redirected separate php-fmp instane (ProxyPass)              | `notfpm`          |


### Deploy via Helm

```console
export HELM_EXPERIMENTAL_OCI=1
helm chart pull registry.gitlab.com/kubelink/httpd:helm-chart
helm chart export registry.gitlab.com/kubelink/httpd:helm-chart .
helm install [releasename] httpd 

hs install apache /progdata/code/httpd/helm-chart \
        --namespace default \
        --set ingress.hostname=ks1.sites.kubelink.com
```