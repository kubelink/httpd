FROM rockylinux:9

# Environment
ENV APP_ROOT /webroot
ENV LOG_DIR $APP_ROOT/log
ENV PS1 "\n\n>> httpd \W \$ "
ENV ONDOCKER 1
WORKDIR $APP_ROOT
ENV PATH $APP_ROOT/bin:$PATH
ENV COMPOSER_HOME=/opt/composer
ENV TERM=xterm
ENV PHP_FPM_HOST=notfpm

# Package Repositories
RUN dnf install -y \
            dnf-utils \
            epel-release \
    && dnf -y --setopt=tsflags=nodocs update \
    && dnf -y clean all

# Packages
ENV PACKAGES \
    httpd \
    findutils

RUN dnf -y --setopt=tsflags=nodocs install $PACKAGES \
    && dnf clean all

# Remove some unnecessary files
RUN rm \
    /etc/httpd/conf.d/welcome.conf \
    /etc/httpd/conf.d/userdir.conf \
    /etc/httpd/conf.d/autoindex.conf
#    /etc/httpd/conf.d/ssl.conf


## Set permissions
RUN chown -R 1001:root -R $APP_ROOT /var/log/httpd



## Entrypoint
COPY resources/entrypoint-web.sh /usr/local/bin/entrypoint-web.sh

# Httpd Server config
COPY resources/httpd.conf /etc/httpd/conf/httpd.conf
RUN rm -rf /run/httpd && mkdir /run/httpd && chmod -R a+rwx /run/httpd

# Copy sample index.html
COPY webroot/index.html $APP_ROOT/

## Set permissions for application
RUN chown -R 1001:root /var/log /run /var/cache/httpd /usr/local/bin/entrypoint-web.sh
RUN chmod -R g=u /etc/passwd /var/log /run /usr/local/bin/*
RUN chmod u+x  /usr/local/bin/entrypoint-web.sh  /usr/local/bin/*


# Ready
USER 1001
EXPOSE 8080 8443
CMD ["/usr/local/bin/entrypoint-web.sh"]
